#!/bin/bash

# Abort and clean up if there are no commits to review
# https://unix.stackexchange.com/a/480737
if [[ -n $(sed -n '/^noop/p;q' $1) ]]; then
    printf "\n"
    git rebase --quit # Correctly aborts the rebase
    echo "+================================================+"
    echo "| ABORTED: No release branch necessary           |"
    echo "+================================================+"
    echo "| There are no commits to review."
    echo "| Are you trying to release in the correct repo?"
    echo "| Has your change already been released?"
    echo "| "
    echo "| Cleaning up the unnecessary release branch..."
    echo -n "| "  && git deletecurrentbranch
    echo "+================================================+"
    exit 1
fi

# Used in the "releasebranch" alias to force carefully selecting only approved commits.
# Takes an argument for the path to the 'git-rebase-todo' file.
# This was implemented using the basic idea shown in this blog post:
# https://paul-samuels.com/blog/2019/06/14/git-edit/

echo 'with all commits dropped...'
sed -i -e "s/^pick/drop/g" "$1"

# Extract Jira card IDs into comments for clickable links
# TODO: Query the Jira API to get status to determine pick/drop (very complicated)
sed -i -e 's|.*[^A-Z]\([A-Z]\{2,5\}-[0-9]\{1,6\}\).*|\0\t# https://ssiagvance.atlassian.net/browse/\1|g' "$1"

# https://stackoverflow.com/a/30387689
sed -i.old -e '1 i\
# WARNING: Please ensure that QA has announced a successful automation run before releasing!' "$1"

CURRENT_TIME=$(date +%H:%M) # https://unix.stackexchange.com/a/395936
if [[ "$CURRENT_TIME" > "15:30" ]]; then
    # https://stackoverflow.com/a/30387689
    sed -i.old -e '1 i\
# WARNING: Releasing code late in the day should be avoided when possible!' "$1"
fi

CURRENT_DAY_NUMBER=$(date +%u) # https://stackoverflow.com/a/3490037
if [[ $CURRENT_DAY_NUMBER -gt 4 ]]; then
    sed -i.old -e '1 i\
# WARNING: Releasing code on or before the weekend should be avoided when possible!' "$1"
fi

eval "$(git var GIT_EDITOR)" "$1" # Open the sequence with the usual editor

echo "Starting rebase with the following commands:"
echo "------- $1"
sed -E '/^[[:blank:]]*(#|$)/d' $1 ## Print all non-commented lines, see https://stackoverflow.com/a/42855071
sed -nE '/^# Rebase/p' $1 ## Print just the Rebase details comment line, see https://stackoverflow.com/a/60996026
echo "-------"